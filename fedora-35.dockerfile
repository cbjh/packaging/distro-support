FROM fedora:35

RUN dnf update -y

# GCC 11.3.1

RUN dnf install -y @development-tools g++ cmake

# CUDA 11.7

RUN dnf install 'dnf-command(config-manager)' -y
RUN dnf config-manager --add-repo "https://developer.download.nvidia.com/compute/cuda/repos/fedora35/x86_64/cuda-fedora35.repo"
RUN dnf install -y cuda

RUN useradd -m user
USER user
WORKDIR /home/user

# Install GROMACS-SWAXS 2022

ARG DIR="gromacs-swaxs-2022"
RUN git clone --depth 1 --branch release-2022.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2021

ARG DIR="gromacs-swaxs-2021"
RUN git clone --depth 1 --branch release-2021.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2020

ARG DIR="gromacs-swaxs-2020"
RUN git clone --depth 1 --branch release-2020.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2019

ARG DIR="gromacs-swaxs-2019"
RUN git clone --depth 1 --branch release-2019.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2018

ARG DIR="gromacs-swaxs-2018"
RUN git clone --depth 1 --branch release-2018.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DGMX_SIMD=AVX2_256 -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6
