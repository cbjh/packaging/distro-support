FROM archlinux:base

RUN pacman --noconfirm -Syu

# GCC 13.1.1 as gcc, GCC 12.2.1 as gcc-12

RUN pacman --noconfirm -S --needed base-devel
RUN pacman --noconfirm -S gcc12 cmake git

# CUDA 12.1

RUN pacman --noconfirm -S cuda

RUN useradd -m user
USER user
WORKDIR /home/user

# Install GROMACS-SWAXS 2022 (no CUDA)

ARG DIR="gromacs-swaxs-2022"

RUN git clone --depth 1 --branch release-2022.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU=OFF -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DCMAKE_C_COMPILER=gcc-12 -DCMAKE_CXX_COMPILER=g++-12 -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -- -j 6


# Install GROMACS-SWAXS 2021

ARG DIR="gromacs-swaxs-2021"

RUN git clone --depth 1 --branch release-2021.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DCMAKE_C_COMPILER=gcc-12 -DCMAKE_CXX_COMPILER=g++-12 -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -- -j 6

# Install GROMACS-SWAXS 2020

ARG DIR="gromacs-swaxs-2020"

RUN git clone --depth 1 --branch release-2020.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DCMAKE_C_COMPILER=gcc-12 -DCMAKE_CXX_COMPILER=g++-12 -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -- -j 6

# Install GROMACS-SWAXS 2019

ARG DIR="gromacs-swaxs-2019"

RUN git clone --depth 1 --branch release-2019.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DCMAKE_C_COMPILER=gcc-12 -DCMAKE_CXX_COMPILER=g++-12 -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -- -j 6

# Install GROMACS-SWAXS 2018 (no CUDA)

ARG DIR="gromacs-swaxs-2018"

RUN git clone --depth 1 --branch release-2018.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU=OFF -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DGMX_SIMD=AVX2_256 -DCMAKE_C_COMPILER=gcc-12 -DCMAKE_CXX_COMPILER=g++-12 -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -- -j 6
