# Run

Docker scripts in this repository are intended to be run as GitLab pipelines on a weekly schedule.

# Development

You can manually run any of the scripts, example of Ubuntu below

```bash
docker build -f ubuntu-21.10.dockerfile -t temp-ubuntu-21.10 .
docker run -it temp-ubuntu-21.10
```

Press `Ctrl+D` to exit.

Look into `.gitlab-ci.yml` to see how to build the other images.

Add `--no-cache` to `docker build` command in order to force-rebuild selected image.

## FFTW

When installing GROMACS, it is usually recommended to use `-DGMX_BUILD_OWN_FFTW=ON` switch and scripts in this project are using it.

But this makes debugging building of GROMACS more difficult, because it takes time. First FFTW builds for few minutes and only then actual GROMACS build fails.

It is possible to install FFTW3 library and remove `-DGMX_BUILD_OWN_FFTW=ON` to speed up the process.

In Ubuntu and Debian Docker scripts, add:

```bash
RUN apt-get install -y libfftw3-dev
```

It should be added close to the failing line to avoid invalidation of the Docker cache.

# CUDA

## Supported distributions

Nvidia provides CUDA packages for following distributions:  
https://developer.download.nvidia.com/compute/cuda/repos

## Running an image

Tests in this project are only intended to check if _GROMACS-SWAXS_ and _GROMACS Chain Coordinate_ build. There is no need to run resulting Docker images.

That being said, it is possible to run an image if needed, but addition switches are necessary when access to GPU is needed.

Example of running Ubuntu 20.04 image on Ubuntu 21.10

```bash
docker run \
 --device /dev/nvidia0:/dev/nvidia0 \
 --device /dev/nvidiactl:/dev/nvidiactl \
 --device /dev/nvidia-modeset:/dev/nvidia-modeset \
 --device /dev/nvidia-uvm:/dev/nvidia-uvm \
 --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools \
 --gpus all -it temp-ubuntu-20.04 nvidia-smi
```

**Notes**:
  * `--device` entries are needed because of lack of support for `cgroup2` on the Docker side. It should be fixed over time.
  * To test Docker setup itself, you can try using official Nvidia image, for example `docker run --gpus all -it nvidia/cuda:10.1-base nvidia-smi`.
