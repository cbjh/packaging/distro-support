FROM opensuse/leap:15.4

RUN zypper refresh
RUN zypper --non-interactive update

# GCC 7.5.0 as gcc

RUN zypper install -y -t pattern devel_basis
RUN zypper install -y cmake git

# # Nvidia driver 515
#
# RUN zypper addrepo --refresh https://download.nvidia.com/opensuse/leap/15.4 NVIDIA
# RUN zypper --gpg-auto-import-keys refresh
# RUN zypper install -y --auto-agree-with-licenses x11-video-nvidiaG06

# CUDA 11.7 as /usr/local/cuda-11

RUN zypper addrepo -p 100 --refresh https://developer.download.nvidia.com/compute/cuda/repos/opensuse15/x86_64/cuda-opensuse15.repo
RUN zypper --gpg-auto-import-keys refresh
RUN zypper install -y cuda-libraries-11-7 cuda-libraries-devel-11-7 cuda-compiler-11-7 cuda-nvprof-11-7

RUN useradd -m user
USER user
WORKDIR /home/user

# Install GROMACS-SWAXS 2022

ARG DIR="gromacs-swaxs-2022"

RUN git clone --depth 1 --branch release-2022.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU=cuda -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2021

ARG DIR="gromacs-swaxs-2021"

RUN git clone --depth 1 --branch release-2021.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU=cuda -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2020

ARG DIR="gromacs-swaxs-2020"

RUN git clone --depth 1 --branch release-2020.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU=cuda -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2019

ARG DIR="gromacs-swaxs-2019"

RUN git clone --depth 1 --branch release-2019.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU=cuda -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2018

ARG DIR="gromacs-swaxs-2018"

RUN git clone --depth 1 --branch release-2018.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU=cuda -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DGMX_SIMD=AVX2_256 -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6
