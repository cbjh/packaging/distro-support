FROM archlinux

RUN pacman --noconfirm -Syu
RUN pacman --noconfirm -Sy base-devel
RUN pacman --noconfirm -Sy git
RUN pacman --noconfirm -Sy python python-pip python-setuptools

RUN useradd -m user
USER user
WORKDIR /home/user

#
# Spack
#

RUN mkdir -p ~/.spack \
  && git clone --depth 1 https://github.com/spack/spack.git ~/.spack/root

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack spec gcc@11.3.0+binutils

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install --only dependencies gcc@11.3.0+binutils

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install gcc@11.3.0+binutils

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack load gcc@11.3.0 \
  && spack compiler find

## Not needed, just shows a list of available compilers for easier debugging
RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack compilers

## Selects GCC 11.3 to be Spack's default
## Explicitly calling `bash`, because `sh` is the default in Docker and `echo` behaves differently in `bash` and `sh`.
RUN [ "bash", "-c", "echo -e 'packages:\n  all:\n    compiler: [gcc@11.3.0]' > ~/.spack/packages.yaml" ]

## Spack is unpredictable when reusing packages
RUN [ "bash", "-c", "echo -e 'concretizer:\n  reuse: false' > ~/.spack/concretizer.yaml" ]

RUN echo "source ~/.spack/root/share/spack/setup-env.sh" > ~/.bashrc

#
# GROMACS-SWAXS
#

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack spec gromacs@2022:2022+cuda~mpi ^fftw~mpi

## We install dependencies separately for faster debugging, you can skip this step

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install --only dependencies gromacs@:2022+cuda~mpi ^fftw~mpi

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install --only dependencies gromacs@:2021+cuda~mpi ^fftw~mpi

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install --only dependencies gromacs@:2019+cuda~mpi ^fftw~mpi

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install --only dependencies gromacs@:2018+cuda~mpi ^fftw~mpi

# GROMACS-SWAXS

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install gromacs-swaxs@2021.5-0.4+cuda~mpi ^fftw~mpi

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install gromacs-swaxs@2020.7-0.4+cuda~mpi ^fftw~mpi

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install gromacs-swaxs@2019.6-0.3+cuda~mpi ^fftw~mpi

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install gromacs-swaxs@2018.8-0.4+cuda~mpi ^fftw~mpi

# GROMACS Chain Coordinate

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install gromacs-chain-coordinate@2021.5-0.2+cuda~mpi ^fftw~mpi
