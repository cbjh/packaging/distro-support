FROM debian:11

# https://serverfault.com/a/797318
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y
RUN apt-get upgrade -y

# GCC 10.2.1 as gcc

RUN apt-get install -y build-essential cmake git

# CUDA 11.2 + GCC 10.2.1 as gcc-10

RUN apt-get install -y software-properties-common
RUN apt-add-repository contrib
RUN apt-add-repository non-free
RUN apt-get update -y
RUN apt-get install -y --no-install-recommends nvidia-cuda-toolkit

RUN useradd -m user
USER user
WORKDIR /home/user

# As of 26.02.2023, GROMACS 2022 and 2021 don't build with the default GCC 10.

USER root

RUN apt-get install -y gcc-9 g++-9

USER user


# Install GROMACS-SWAXS 2022

ARG DIR="gromacs-swaxs-2022"

RUN git clone --depth 1 --branch release-2022.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DCMAKE_C_COMPILER=gcc-9 -DCMAKE_CXX_COMPILER=g++-9 -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2021

ARG DIR="gromacs-swaxs-2021"

RUN git clone --depth 1 --branch release-2021.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DCMAKE_C_COMPILER=gcc-9 -DCMAKE_CXX_COMPILER=g++-9 -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2020

ARG DIR="gromacs-swaxs-2020"

RUN git clone --depth 1 --branch release-2020.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2019

ARG DIR="gromacs-swaxs-2019"

RUN git clone --depth 1 --branch release-2019.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2018

ARG DIR="gromacs-swaxs-2018"

RUN git clone --depth 1 --branch release-2018.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DGMX_SIMD=AVX2_256 -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6
