FROM ubuntu:20.04

# https://serverfault.com/a/797318
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y
RUN apt-get upgrade -y

# GCC 9.4.0 as gcc
RUN apt-get install -y build-essential cmake git

#
# Ubuntu 20.04 offers CUDA 10.1 which is not supported by GROMACS 2022
#

RUN apt-get install -y wget

RUN wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-keyring_1.0-1_all.deb \
    && dpkg -i cuda-keyring_1.0-1_all.deb \
    && rm cuda-keyring_1.0-1_all.deb \
    && apt-get update -y

RUN apt-get install -y --no-install-recommends cuda-toolkit-11-8

RUN useradd -m user
USER user
WORKDIR /home/user

# Install GROMACS-SWAXS 2022

ARG DIR="gromacs-swaxs-2022"

RUN git clone --depth 1 --branch release-2022.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2021

ARG DIR="gromacs-swaxs-2021"

RUN git clone --depth 1 --branch release-2021.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2020

ARG DIR="gromacs-swaxs-2020"

RUN git clone --depth 1 --branch release-2020.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2019

ARG DIR="gromacs-swaxs-2019"

RUN git clone --depth 1 --branch release-2019.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6

# Install GROMACS-SWAXS 2018

ARG DIR="gromacs-swaxs-2018"

RUN git clone --depth 1 --branch release-2018.swaxs https://gitlab.com/cbjh/gromacs-swaxs.git "${DIR}-src" \
    && cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DGMX_SIMD=AVX2_256 -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j 6
